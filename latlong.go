package main

import (
	"math"
)

const EarthRadius = 6373 // kilometers

func DistanceLatLong(latA, longA, latB, longB float64) float64 {

	latA = latA * math.Pi / 180.0
	latB = latB * math.Pi / 180.0
	longA = longA * math.Pi / 180.0
	longB = longB * math.Pi / 180.0

	dlon := longB - longA
	dlat := latB - latA
	a := (math.Sin(dlat/2) * math.Sin(dlat/2)) + math.Cos(latA)*math.Cos(latB)*(math.Sin(dlon/2))*math.Sin(dlon/2)
	c := 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))
	d := EarthRadius * c

	return d
}
