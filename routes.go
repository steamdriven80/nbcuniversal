package main

import (
	"github.com/gorilla/mux"
	"net/http"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var routes = Routes{
	Route{
		"GuestIndex",
		"GET",
		"/guests",
		GuestIndex,
	},
	Route{
		"MissionIndex",
		"GET",
		"/missions",
		MissionIndex,
	},
	Route{
		"GuestShow",
		"GET",
		"/guests/{id}",
		GuestShow,
	},
	Route{
		"MissionShow",
		"GET",
		"/missions/{id}",
		MissionShow,
	},
	Route{
		"ExpressPassShow",
		"GET",
		"/passes/{id}",
		ExpressPassShow,
	},
	Route{
		"GuestCreate",
		"POST",
		"/guests",
		GuestCreate,
	},
	Route{
		"MissionPoll",
		"GET",
		"/poll/{id}",
		MissionPollGet,
	},
	Route{
		"MissionPoll",
		"GET",
		"/poll",
		MissionPollGet,
	},
	Route{
		"MissionPollPost",
		"POST",
		"/poll/{id}/{lat}/{long}",
		MissionPollPost,
	},
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
}

type MyServer struct {
	r *mux.Router
}

func (s *MyServer) ServeHTTP(rw http.ResponseWriter, req *http.Request) {
	if origin := req.Header.Get("Origin"); origin != "" {
		rw.Header().Set("Access-Control-Allow-Origin", "*")
		rw.Header().Set("Access-Control-Allow-Methods", "POST, GET, OPTIONS, PUT, DELETE")
		rw.Header().Set("Access-Control-Allow-Headers",
			"Accept, Content-Type, Content-Length, Accept-Encoding, X-CSRF-Token, Authorization")
	}
	// Stop here if its Preflighted OPTIONS request
	if req.Method == "OPTIONS" {
		return
	}
	// Lets Gorilla work
	s.r.ServeHTTP(rw, req)
}
