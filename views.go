package main

import (
	"code.google.com/p/go-uuid/uuid"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"html"
	"io"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"strconv"
	"strings"
	"time"
)

func MissionPollGet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)

	userID, _ := vars["id"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	gen, err := RepoFindItem("guests", userID)
	if err != nil {

		var guest Guest

		guest.ID = html.EscapeString(uuid.New())

		RepoSetItem("guests", guest.ID, MustMarshal(guest))

		w.WriteHeader(http.StatusCreated)
		if err := json.NewEncoder(w).Encode(guest); err != nil {
			w.WriteHeader(500)
			w.Write([]byte(MustMarshal(struct {
				error   string
				message string
			}{"true", err.Error()})))
			return
		}

		return
	}

	var user Guest

	if err = json.Unmarshal([]byte(gen), &user); err != nil {
		panic(err)
	}

	if user.CurrentMission != "" {
		w.WriteHeader(http.StatusOK)

		data := RepoMustFindItem("missions", user.CurrentMission)

		w.Write([]byte(MustMarshal(data)))

		return
	}

	//delta := user.LastPing.Unix() - time.Now().Unix()

	//chanceForMission := 0.1 * 100

	//if float64(delta)*chanceForMission < 900.0*rand.Float64()+100.0 {

	mission := Mission{
		ID:             html.EscapeString(uuid.New()),
		StartTime:      time.Now(),
		LastPing:       time.Now(),
		TargetDistance: float64(900.0*rand.Float32() + 100.0),
		UserID:         user.ID,
	}

	fmt.Println("------------------MISSION-----------------------")
	fmt.Println(mission)

	RepoSetItem("missions", mission.ID, MustMarshal(mission))

	w.WriteHeader(http.StatusOK)
	w.Write([]byte(MustMarshal(mission)))

	user.CurrentMission = mission.ID
	user.NumOfMissions++

	RepoSetItem("guests", user.ID, MustMarshal(user))

	return
	//} else {

	//	w.WriteHeader(http.StatusOK)
	//	w.Write([]byte(MustMarshal(map[string]string{"updates": "false"})))

	//	return
	//}
}

type LatLong struct {
	Latitude  float64
	Longitude float64
}

func MissionPollPost(w http.ResponseWriter, r *http.Request) {
	var latlong LatLong

	vars := mux.Vars(r)

	missionID, _ := vars["id"]
	latlong.Latitude, _ = strconv.ParseFloat(vars["lat"], 64)
	latlong.Longitude, _ = strconv.ParseFloat(vars["long"], 64)

	fmt.Println("%v", latlong)

	mission, err := RepoFindItem("missions", missionID)
	if err != nil {
		w.WriteHeader(500)
		log.Fatal("the mission doesn't exist")
		w.Write([]byte(`{"error":"true", "message": "the mission doesn't exist"}`))
		return
	}

	var object Mission

	json.Unmarshal([]byte(mission), &object)

	user, err := RepoFindItem("guests", object.UserID)
	if err != nil {
		w.WriteHeader(500)
		log.Fatal("the user id does not exist")
		w.Write([]byte(`{"error":"true", "message": "the user id doesn't exist"}`))
		return
	}

	var user_obj Guest

	json.Unmarshal([]byte(user), &user_obj)

	user_obj.LastPing = time.Now()

	//distance := (latlong.Latitude-user_obj.Latitude)*(latlong.Latitude-user_obj.Latitude) + (latlong.Longitude-user_obj.Longitude)*(latlong.Longitude-user_obj.Longitude)

	//sq_distance := math.Sqrt(float64(distance))

	//object.ProgressInFeet = int(sq_distance)

	//object.ProgressInPercent = float64(sq_distance / float64(object.TargetDistance))

	if user_obj.Latitude == 0 {
		user_obj.Latitude = latlong.Latitude
		user_obj.Longitude = latlong.Longitude
	} else {
		object.ProgressInUnits = object.ProgressInUnits + DistanceLatLong(latlong.Latitude, latlong.Longitude, user_obj.Latitude, user_obj.Longitude)
		object.ProgressInPercent = (object.ProgressInUnits / object.TargetDistance) * 100.0
	}

	user_obj.Latitude = latlong.Latitude
	user_obj.Longitude = latlong.Longitude

	object.LastPing = time.Now()

	RepoSetItem("guests", user_obj.ID, MustMarshal(user_obj))
	RepoSetItem("missions", object.ID, MustMarshal(object))

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(MustMarshal(object)))

}

func MissionIndex(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(strings.Replace(MustMarshal(RepoAllItems("mission")), `\`, "", -1)))
}

func MissionShow(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	missionID, _ := vars["id"]

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	data := RepoMustFindItem("missions", missionID)
	w.Write([]byte(data))
}

func GuestIndex(w http.ResponseWriter, r *http.Request) {

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	w.Write([]byte(strings.Replace(MustMarshal(RepoAllItems("guests")), `\`, "", -1)))
}

func GuestShow(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	guestID, _ := vars["id"]

	data := RepoMustFindItem("guests", guestID)
	w.Write([]byte(data))
}

func ExpressPassShow(w http.ResponseWriter, r *http.Request) {

	vars := mux.Vars(r)

	passID, _ := vars["id"]

	data := RepoMustFindItem("passes", passID)
	w.Write([]byte(data))
}

func GuestCreate(w http.ResponseWriter, r *http.Request) {

	var guest Guest

	body, err := ioutil.ReadAll(io.LimitReader(r.Body, 1048576))
	if err != nil {
		panic(err)
	}
	if err := r.Body.Close(); err != nil {
		panic(err)
	}
	if err := json.Unmarshal(body, &guest); err != nil {
		w.Header().Set("Content-Type", "application/json; charset=UTF-8")
		w.WriteHeader(422) // unprocessable entity
		if err := json.NewEncoder(w).Encode(err); err != nil {
			panic(err)
		}
	}

	guest.ID = html.EscapeString(uuid.New())

	RepoSetItem("guests", guest.ID, MustMarshal(guest))

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusCreated)
	if err := json.NewEncoder(w).Encode(guest); err != nil {
		panic(err)
	}
}

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, `
	<html>
	<head>
		<title>Universal App</title>
	</head>
	<body>
		<div class="container">
			<a href="/guests"><h3>Guest List</h3></a>
			<a href="/missions"><h3>Mission List</h3></a>
			<a href="/passes"><h3>Express Pass List</h3></a>
		</div>
	</body>
	</html>
	`)
}
