package main

import (
	"log"
	"net/http"
)

func main() {

	RepoInit()

	router := NewRouter()

	log.Fatal(http.ListenAndServe(":8080", router))
}
