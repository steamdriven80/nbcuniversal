package main

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/boltdb/bolt"
)

var database *bolt.DB

func MustMarshal(item interface{}) string {
	data, err := json.Marshal(item)
	if err != nil {
		panic(err)
	}

	return string(data)
}

// Give us some seed data
func RepoInit() {
	db, err := bolt.Open("blog.db", 0600, nil)
	if err != nil {
		log.Fatal(err)
	}

	database = db

	RepoNew("guests")
	RepoNew("missions")
	RepoNew("passes")

	//	for i := 0; i < 5; i++ {
	//		id := uuid.New()

	//		RepoSetItem("mission", id, MustMarshal(Mission{
	//			ID:             id,
	//			StartTime:      time.Now(),
	//			TargetDistance: 900.0*rand.Float32() + 100,
	//		}))
	//	}

	//	for i := 0; i < 5; i++ {
	//		id := uuid.New()

	//		RepoSetItem("guests", id, MustMarshal(Guest{
	//			ID: id,
	//		}))
	//	}guests
}

func RepoNew(db_name string) error {

	err := database.Update(func(tx *bolt.Tx) error {
		_, err := tx.CreateBucketIfNotExists([]byte("db"))
		if err != nil {
			return err
		}

		return nil
	})

	return err
}

func RepoFindItem(db_name string, id string) (string, error) {
	var item string /*
		var found bool*/

	err := database.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("db"))
		if err != nil {
			return err
		}
		if b == nil {
			return fmt.Errorf("invalid bucket object!")
		}
		//		b.ForEach(func(k, v []byte) error {
		//			if string(k) == id {
		//				item = string(v)
		//				found = true
		//			}
		//			return nil
		//		})

		//		if !found {
		item = string(b.Get([]byte(id)))
		//			return fmt.Errorf("database ID not found")
		//		}

		return nil
	})

	//fmt.Println("FindItem: ", item, err)

	if len(item) == 0 {
		err = fmt.Errorf("database ID not found")
	}

	return item, err
}

func RepoMustFindItem(db_name string, id string) string {

	item, _ := RepoFindItem("db", id)

	return item
}

func RepoAllItems(db_name string) map[string]string {

	items := make(map[string]string)

	database.Update(func(tx *bolt.Tx) error {
		b := tx.Bucket([]byte("db"))
		if b == nil {
			return fmt.Errorf("bucket does not exist")
		}
		b.ForEach(func(k, v []byte) error {
			items[string(k)] = string(v)

			return nil
		})

		return nil
	})

	fmt.Println("FindAllItem: ", items)

	return items
}

func RepoSetItem(db_name string, id string, data string) error {
	err := database.Update(func(tx *bolt.Tx) error {
		b, err := tx.CreateBucketIfNotExists([]byte("db"))
		if err != nil {
			return err
		}

		fmt.Println("SetItem: ", id, data)

		return b.Put([]byte(id), []byte(data))
	})

	return err
}

func RepoDestroyItem(db_name string, id string) error {
	//	var item string
	//	var found bool

	//	err := database.Update(func(tx *bolt.Tx) error {
	//		b, err := tx.CreateBucketIfNotExists([]byte(db_name))
	//		if err != nil {
	//			return err
	//		}
	//		b.ForEach(func(k, v []byte) error {
	//			if string(k) == id {
	//				item = string(v)
	//				found = true
	//			}
	//			return nil
	//		})

	//		if !found {
	//			return fmt.Errorf("database ID not found")
	//		}

	//		return nil
	//	})

	//	return err

	return fmt.Errorf("not implemented")
}
