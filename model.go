package main

import "time"

type Guest struct {
	// unique guest ID
	ID string `json: id`

	// user's last latitude
	Latitude float64 `json: latitude`

	// user's last longitude
	Longitude float64 `json: longitude`

	// total distance travelled
	TotalDistance float64 `json: totalDistance`

	// the last time we saw this guest
	LastPing time.Time `json: lastPing`

	// the total number of missions given throughout the day
	NumOfMissions int `json: numOfMissions`

	// the current goal assigned to this guest
	CurrentMission string `json: currentMission`

	// the express passes earned by the guest
	ExpressPasses []ExpressPass `json: passes`
}

type Guests []Guest

type Mission struct {
	// a unique ID to identity this mission
	ID string `json: id`

	// the start time of the mission
	StartTime time.Time `json: startTime`

	// the end time of the mission
	EndTime time.Time `json: endTime`

	// target # of steps
	TargetDistance float64 `json: targetDistance`

	// ID of the guest this mission is assigned
	UserID string `json: userID`

	// time of last ping we received from the user
	LastPing time.Time `json: lastPing`

	// # of steps reported by the user
	ProgressInUnits float64 `json: progressInUnits`

	// progress of user (0-100%) as a float 0.0 - 1.0
	ProgressInPercent float64 `json: progressInPercent`
}

type Missions []Mission

type ExpressPass struct {
	// a unique ID to identify the express pass
	ID string `json: id`

	// the guest this pass is assigned to
	UserID string `json: user`

	// barcode to identify this express pass
	Barcode string `json: barcode`
}

func (p ExpressPass) GetID() string {
	return p.ID
}

func (p Guest) GetID() string {
	return p.ID
}

func (p Mission) GetID() string {
	return p.ID
}
